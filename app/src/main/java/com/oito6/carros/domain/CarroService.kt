package com.oito6.carros.domain

import com.oito6.carros.domain.dao.DatabaseManager
import com.oito6.carros.domain.retrofit.CarrosREST
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object CarroService {

    private val BASE_URL = "http://livrowebservices.com.br/rest/carros/"
    private var service: CarrosREST

    init {
        val retrofit = Retrofit
                .Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()

        service = retrofit.create(CarrosREST::class.java)
    }

    // Busca os carros por tipo (clássicos, esportivos ou luxo)
    fun getCarros(tipo: TipoCarro): List<Carro> {

        val call = service.getCarros(tipo.name)
        val carros = call.execute().body()

        return carros!!
    }

    // Salva um carro
    fun save(carro: Carro): Response {

        val call = service.save(carro)
        val response = call.execute().body()

        return response!!
    }

    // Deleta um carro
    fun delete(carro: Carro): Response {

        val call = service.delete(carro.id)
        val response = call.execute().body()

        if (response?.isOk()!!) {
            // Se removeu do servidor, remove dos favoritos
            val dao = DatabaseManager.getCarroDAO()
            dao.delete(carro)
        }

        return response
    }
}