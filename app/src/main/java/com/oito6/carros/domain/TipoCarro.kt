package com.oito6.carros.domain

import com.oito6.carros.R

enum class TipoCarro(
        val string: Int
) {
    classicos(R.string.classicos),
    esportivos(R.string.esportivos),
    luxo(R.string.luxo),
    favoritos(R.string.favoritos)
}