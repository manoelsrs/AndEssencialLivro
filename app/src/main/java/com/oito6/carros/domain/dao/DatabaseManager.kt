package com.oito6.carros.domain.dao

import android.arch.persistence.room.Room
import com.oito6.carros.MyApp

object DatabaseManager {

    // Singleton do Room: banco de dados (pg.367)
    private var dbInstance: CarrosDatabase

    init {
        val appContext = MyApp.getInstance().applicationContext

        // Configura o Room
        dbInstance = Room.databaseBuilder(
                appContext,
                CarrosDatabase::class.java,
                "carros.sqlite"
        ).build()
    }

    fun getCarroDAO(): CarroDAO {
        return dbInstance.carroDAO()
    }
}