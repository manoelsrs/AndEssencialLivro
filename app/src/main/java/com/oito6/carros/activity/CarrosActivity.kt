package com.oito6.carros.activity

import android.os.Bundle
import com.oito6.carros.R
import com.oito6.carros.domain.TipoCarro
import com.oito6.carros.extensions.addFragment
import com.oito6.carros.extensions.setupToolbar
import com.oito6.carros.fragments.CarrosFragment
import kotlinx.android.synthetic.main.activity_carros.*
import kotlinx.android.synthetic.main.toolbar.*

class CarrosActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_carros)

        // Argumentos: tipo do carro (pg.221)
        val tipo = intent.getSerializableExtra("tipo") as TipoCarro
        val title = getString(tipo.string)

        // Toolbar: configura o título e o "up navigation"
        setupToolbar(toolbar.id, title, true)
        if (savedInstanceState == null) {
            // Adiciona o fragment no layout de marcação
            // Dentre os argumentos que foram passados para a Activity, está o tipo carro.
            // A função abaixo está na ActivityExtensions.kt
            addFragment(container.id, CarrosFragment())
        }
    }
}
