package com.oito6.carros.activity

import android.graphics.Bitmap
import android.os.Bundle
import android.view.View
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import com.oito6.carros.R
import com.oito6.carros.activity.dialogs.AboutDialog
import com.oito6.carros.extensions.setupToolbar
import kotlinx.android.synthetic.main.activity_site_livro.*
import kotlinx.android.synthetic.main.toolbar.*

class SiteLivroActivity : BaseActivity() {

    private val URL_SOBRE = "http://www.livroandroid.com.br/sobre.htm"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_site_livro)

        /* Habilitando JavaScript (curiosidade!!)
        val settings = webview.settings
        settings.javaScriptEnabled = true
        webview.loadUrl("javascript:alert('Olá')")*/

        // Toolbar
        val actionBar = setupToolbar(toolbar.id)
        actionBar.setDisplayHomeAsUpEnabled(true)

        // Carrega a página
        setWebClient(webview)
        webview?.loadUrl(URL_SOBRE)

        // Swipe to Refresh
        swipeToRefresh?.setOnRefreshListener {
            webview?.reload()
        }

        // Cores da animação
        swipeToRefresh?.setColorSchemeResources(
                R.color.refresh_progress_1,
                R.color.refresh_progress_2,
                R.color.refresh_progress_3
        )
    }

    private fun setWebClient(webview: WebView?) {
        webview?.webViewClient = object : WebViewClient() {
            override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
                super.onPageStarted(view, url, favicon)

                // Liga o progress
                progress?.visibility = View.VISIBLE
            }

            override fun onPageFinished(view: WebView?, url: String?) {
                super.onPageFinished(view, url)

                // Desliga o progress
                progress?.visibility = View.INVISIBLE

                //Termina a animação do Swipe to Refresh
                swipeToRefresh?.isRefreshing = false
            }

            override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {

                val url = request?.url.toString()

                if (url.endsWith("sobre.htm")) {
                    // Mostra um dialog
                    AboutDialog.showAbout(supportFragmentManager)

                    // Retorna true para informar que interceptamos o evento
                    return true
                }

                return super.shouldOverrideUrlLoading(view, request)
            }
        }
    }
}
