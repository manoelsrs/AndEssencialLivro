package com.oito6.carros.activity

import android.content.Intent
import android.content.res.ColorStateList
import android.net.Uri
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.view.Menu
import android.view.MenuItem
import com.oito6.carros.R
import com.oito6.carros.domain.Carro
import com.oito6.carros.domain.CarroService
import com.oito6.carros.domain.FavoritosService
import com.oito6.carros.domain.Response
import com.oito6.carros.extensions.loadUrl
import com.oito6.carros.extensions.setupToolbar
import com.oito6.carros.scheduler.RxScheduler
import io.reactivex.Observable
import kotlinx.android.synthetic.main.activity_carro.*
import kotlinx.android.synthetic.main.activity_carro_contents.*
import kotlinx.android.synthetic.main.fragment_carros.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.toast

class CarroActivity : BaseActivity() {

    val carro: Carro by lazy {
        intent.getParcelableExtra<Carro>("carro")
    }

    var favoritado: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_carro)

        // Seta o nome do carro como título da Toolbar
        setupToolbar(R.id.toolbar, carro.nome, true)

        loadFabColor()

        // Atualiza os dados do carro na tela
        initViews()

        // Variável gerada automaticamente pelo Kotlin Extensions
        fab.setOnClickListener {
            onClickFavoritar(carro)
        }
    }

    private fun loadFabColor() {
        Observable
                .just(carro)
                .observeOn(RxScheduler().backgroundThread())
                .map {
                    favoritado = FavoritosService.isFavorito(it)
                }
                .observeOn(RxScheduler().mainThread())
                .subscribe {
                    setFavoriteColor(favoritado)
                }
    }

    // Adiciona ou Remove o carro dos Favoritos
    private fun onClickFavoritar(carro: Carro) {

        Observable
                .just(carro)
                .observeOn(RxScheduler().backgroundThread())
                .map {
                    favoritado = FavoritosService.favoritar(it)
                }
                .observeOn(RxScheduler().mainThread())
                .subscribe {
                    // Alerta de Sucesso
                    toast(if (favoritado) R.string.msg_carro_favoritado
                          else R.string.msg_carro_desfavoritado)

                    setFavoriteColor(favoritado)
                }
    }

    private fun initViews() {

        // Variáveis geradas automaticamente pelo Kotlin Extensions (veja import)
        tDesc.text = carro.desc
        appBarImg.loadUrl(carro.urlFoto)

        // Foto do Carro (pequena com transparência)
        img.loadUrl(carro.urlFoto)

        // Toca o Vídeo
        imgPlayVideo.setOnClickListener {
            val url = carro.urlVideo
            val intent = Intent(Intent.ACTION_VIEW)
            intent.setDataAndType(Uri.parse(url), "video/*")
            startActivity(intent)
        }
    }

    // Desenha a cor do FAB conforme o status do favoritos ou não.
    private fun setFavoriteColor(favorito: Boolean) {

        // Troca a cor conforme o status do favoritos
        val fundo = ContextCompat.getColor(
                this,
                if (favorito) R.color.favorito_on
                else R.color.favorito_off
        )

        val cor = ContextCompat.getColor(
                this,
                if (favorito) R.color.yellow
                else R.color.favorito_on
        )

        fab.backgroundTintList = ColorStateList(arrayOf(intArrayOf(0)), intArrayOf(fundo))
        fab.setColorFilter(cor)
    }

    // Adiciona as opções Salvar e Deletar no menu (pg.342)
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {

        menuInflater.inflate(R.menu.menu_carro, menu)

        return true
    }

    // Trata os eventos do menu (pg.342)
    override fun onOptionsItemSelected(item: MenuItem?): Boolean {

        when(item?.itemId) {
            R.id.action_editar -> {
                startActivity<CarroFormActivity>("carro" to carro)
                finish()
            }

            R.id.action_deletar -> {
                // pg.351
                alert(R.string.msg_confirma_excluir_carro, R.string.app_name) {
                    positiveButton(R.string.sim) {
                        // Confirmou o excluir
                        taskExcluir()
                    }

                    negativeButton(R.string.nao) {
                        // Não confirmou
                    }
                }.show()
            }
        }

        return super.onOptionsItemSelected(item)
    }

    // Exclui um carro do servidor
    private fun taskExcluir() {

        var response: Response? = null

        Observable
                .just(carro)
                .observeOn(RxScheduler().backgroundThread())
                .map {
                    response = CarroService.delete(it)
                }
                .observeOn(RxScheduler().mainThread())
                .subscribe {
                    toast(response?.msg ?: "Erro no recebimento da mensagem do servidor!")
                    finish()
                }
    }
}
