package com.oito6.carros.activity

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import com.oito6.carros.R
import com.oito6.carros.domain.Carro
import com.oito6.carros.domain.CarroService
import com.oito6.carros.domain.Response
import com.oito6.carros.domain.TipoCarro
import com.oito6.carros.extensions.isEmpty
import com.oito6.carros.extensions.loadUrl
import com.oito6.carros.extensions.setupToolbar
import com.oito6.carros.extensions.string
import com.oito6.carros.scheduler.RxScheduler
import io.reactivex.Observable
import kotlinx.android.synthetic.main.activity_carro_form.*
import kotlinx.android.synthetic.main.activity_carro_form_contents.*
import org.jetbrains.anko.toast

class CarroFormActivity : BaseActivity() {

    // O carro pode ser nulo no caso de um novo Carro
    val carro: Carro? by lazy {
        intent.getParcelableExtra<Carro>("carro")
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_carro_form)

        // Título da Toolbar (Nome do carro ou Novo Carro) pg.347
        setupToolbar(R.id.toolbar, carro?.nome ?: getString(R.string.novo_carro))

        // Atualiza os dados do formulário
        initViews()
    }

    // Inicializa as views
    private fun initViews() {

        // A função apply somente é executada se o objeto não for nulo
        carro?.apply {

            // Foto do carro
            appBarImg.loadUrl(carro?.urlFoto)

            // Dados do carro
            tDesc.string = desc
            tNome.string = nome

            // Tipo do carro
            when(tipo) {
                "classicos" -> radioTipo.check(R.id.tipoClassico)
                "esportivos" -> radioTipo.check(R.id.tipoEsportivo)
                "luxo" -> radioTipo.check(R.id.tipoLuxo)
            }
        }
    }

    // Adiciona as opções Salvar e Deletar no menu
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {

        menuInflater.inflate(R.menu.menu_form_carro, menu)

        return true
    }

    // Trata os eventos do menu (pg.348)
    override fun onOptionsItemSelected(item: MenuItem?): Boolean {

        when(item?.itemId) {
            R.id.action_salvar -> taskSalvar()
        }

        return super.onOptionsItemSelected(item)
    }

    // Salva o carro no ws
    private fun taskSalvar() {

        if (tNome.isEmpty()) {

            // Valida se o campo descrição foi preenchido
            tNome.error = getString(R.string.msg_error_form_nome)
            return
        }

        if (tDesc.isEmpty()) {

            // Valida se o campo descrição foi preenchido
            tDesc.error = getString(R.string.msg_error_form_desc)
            return
        }

        // Usando RxKotlin, diferentemente do livro
        var response: Response? = null

        // Cria um carro para salvar/atualizar
        val c = carro ?: Carro()

        Observable
                .just(c)
                .observeOn(RxScheduler().backgroundThread())
                .map {

                    // Copia valores do form para o Carro
                    it.nome = tNome.string
                    it.desc = tDesc.string
                    it.tipo = when(radioTipo.checkedRadioButtonId) {
                        R.id.tipoClassico -> TipoCarro.classicos.name
                        R.id.tipoEsportivo -> TipoCarro.esportivos.name
                        else -> TipoCarro.luxo.name
                    }

                    // Salva no servidor
                    response = CarroService.save(it)
                }
                .observeOn(RxScheduler().mainThread())
                .subscribe {

                    // Mensagem com a resposta do servidor
                    toast(response?.msg!!)
                    finish()
                }

    }
}
