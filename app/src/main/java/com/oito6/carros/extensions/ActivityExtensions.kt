package com.oito6.carros.extensions

import android.support.annotation.IdRes
import android.support.v4.app.Fragment
import android.support.v7.app.ActionBar
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar

// findViewById + setOnClickListener -> NÃO FOI ADICIONADO pg.206

// Mostra um toast -> NÃO PRECISA MAIS, ANKO COMMONS FAZ ISSO pg.227

// Configura a Toolbar
fun AppCompatActivity.setupToolbar(
        @IdRes id: Int,
        title: String? = null,
        upNavigation: Boolean = false): ActionBar {

    val toolbar = findViewById<Toolbar>(id)
    setSupportActionBar(toolbar)

    if (title != null) {
        supportActionBar?.title = title
    }

    supportActionBar?.setDisplayHomeAsUpEnabled(upNavigation)

    return supportActionBar!!
}

// Adiciona o fragment no layout
fun AppCompatActivity.addFragment(@IdRes layoutId: Int, fragment: Fragment) {

    fragment.arguments = intent.extras
    val ft = supportFragmentManager.beginTransaction()
    ft.add(layoutId, fragment)
    ft.commit()
}