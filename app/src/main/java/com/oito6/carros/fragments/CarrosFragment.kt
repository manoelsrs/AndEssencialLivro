package com.oito6.carros.fragments

import android.os.Bundle
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.oito6.carros.R
import com.oito6.carros.activity.CarroActivity
import com.oito6.carros.adapter.CarroAdapter
import com.oito6.carros.domain.Carro
import com.oito6.carros.domain.CarroService
import com.oito6.carros.domain.TipoCarro
import com.oito6.carros.extensions.toast
import com.oito6.carros.scheduler.RxScheduler
import com.oito6.carros.utils.AndroidUtils
import io.reactivex.Observable
import kotlinx.android.synthetic.main.fragment_carros.*
import kotlinx.android.synthetic.main.fragment_carros.view.*
import org.jetbrains.anko.startActivity

open class CarrosFragment : BaseFragment() {

    private var tipo = TipoCarro.classicos
    protected var carros = listOf<Carro>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // Lê o parâmetro tipo enviado(clássicos, esportivos ou luxo)
        if (arguments != null) {
            tipo = arguments?.getSerializable("tipo") as TipoCarro
        }
    }

    // Cria a view do fragment
    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?): View? {

        // Retorna a view /res/layout/fragment_carros.xml
        val view = inflater.inflate(R.layout.fragment_carros,container,false)

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //Views
        recyclerView.layoutManager = LinearLayoutManager(activity)
        recyclerView.itemAnimator = DefaultItemAnimator()
        recyclerView.setHasFixedSize(true)
    }

    override fun onResume() {
        super.onResume()

        if (AndroidUtils.isNetworkAvailable(context!!)) {
            taskCarros()
        } else {
            toast("Sem internet!!")
        }
    }

    open fun taskCarros() {

        Observable
                .just(carros)
                .observeOn(RxScheduler().backgroundThread())
                .map {
                    carros = CarroService.getCarros(tipo)
                }
                .observeOn(RxScheduler().mainThread())
                .subscribe {
                    recyclerView.adapter = CarroAdapter(carros) { onClickCarro(it)}
                }
    }

    open fun onClickCarro(carro: Carro) {

        // Ao clicar no carro vamos navegar para a tela de detalhes
        activity?.startActivity<CarroActivity>("carro" to carro)
    }
}
