package com.oito6.carros.fragments

import com.oito6.carros.activity.CarroActivity
import com.oito6.carros.adapter.CarroAdapter
import com.oito6.carros.domain.Carro
import com.oito6.carros.domain.FavoritosService
import com.oito6.carros.scheduler.RxScheduler
import io.reactivex.Observable
import kotlinx.android.synthetic.main.fragment_carros.*
import org.jetbrains.anko.startActivity

class FavoritosFragment : CarrosFragment() {

    override fun taskCarros() {

        Observable
                .just(carros)
                .observeOn(RxScheduler().backgroundThread())
                .map {
                    carros = FavoritosService.getCarros()
                }
                .observeOn(RxScheduler().mainThread())
                .subscribe {
                    recyclerView.adapter = CarroAdapter(carros) { onClickCarro(it)}
                }
    }

    override fun onClickCarro(carro: Carro) {

        // Ao clicar no carro vamos navegar para a tela de detalhes
        activity?.startActivity<CarroActivity>("carro" to carro)
    }
}