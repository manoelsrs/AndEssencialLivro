package com.oito6.carros

import android.app.Application
import android.util.Log

class MyApp : Application() {
    private val TAG = "MyApp"

    companion object {
        //Singleton da classe Aplication
        private var appInstance: MyApp? = null

        fun getInstance(): MyApp {
            if (appInstance == null) {
                throw IllegalStateException("Configure a classe de Application no Manifest")
            }
            return appInstance!!
        }
    }

    override fun onCreate() {
        super.onCreate()

        //Salva a instância para termos acesso como Singleton
        appInstance = this
    }

    override fun onTerminate() {
        super.onTerminate()
        Log.d(TAG, "MyApp.onTerminate()")
    }
}