package com.oito6.carros.scheduler

import io.reactivex.Scheduler

interface BaseScheduler {

    fun mainThread(): Scheduler

    fun backgroundThread(): Scheduler
}